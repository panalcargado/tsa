class BranchOffice < ApplicationRecord
  belongs_to :client
  has_many :branch_office_addresses
end
