class BranchOfficeAddress < ApplicationRecord
  belongs_to :city
  belongs_to :branch_office
end
