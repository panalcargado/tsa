class User < ApplicationRecord
  has_many :user_tasks
  has_many :tasks, :through => :user_tasks
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :confirmable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  mount_uploader :avatar, AvatarUploader
  extend Enumerize
  enumerize :role, in:[:'root', :'admin', :'user'], default: :'user'

end
