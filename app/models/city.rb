class City < ApplicationRecord
	belongs_to :state
	has_one :client_address
	has_one :branch_office_address
end
