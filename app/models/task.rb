class Task < ApplicationRecord
	has_many :user_tasks
	has_many :users, :through => :user_tasks

	#attr_reader :users

	after_create :save_users

	#Custom setter
	def users=(value)
		@users = value
	end

	private

	def save_users
		@users.each do |user_id|
			UserTask.create(user_id: user_id, task_id: self.id)
		end
	end
end
