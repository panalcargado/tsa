class Client < ApplicationRecord
	has_many :client_addresses, :dependent => :destroy
	has_many :branch_offices, :dependent => :destroy


	accepts_nested_attributes_for :client_addresses, :allow_destroy => true


	validates_presence_of :first_name, :last_name, 
		:message => "Este campo es obligatorio."
	validates :dni_number, :cuil_number, 
		numericality: { message: "Se permite solo numeros enteros."}, 
		:allow_nil => true
	#validates_length_of :dni_number, :minimum => 8, :maximum => 8, :message => "Ingrese un DNI válido. (mínimo 8 caracteres numericos, sin puntos ni comas)"
	#validates_length_of :cuil_number, :minimum => 11, :maximum => 11, :message => "Ingrese un CUIL válido. (mínimo 11 caracteres numericos, sin puntos ni comas)"
	validates :email, 
		format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, message: "formato email@email.com"  }, 
		:allow_nil => true

end