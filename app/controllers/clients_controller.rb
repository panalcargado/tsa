class ClientsController < ApplicationController
  before_action :set_client, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token
  respond_to :html, :json


  # GET /clients
  # GET /clients.json
  def index
    @clients = Client.all
  end

  # GET /clients/1
  # GET /clients/1.json
  def show
    respond_modal_with @client
  end

  # GET /clients/new
  def new
    @client = Client.new
    # 1.times do 
    #   address = @client.client_addresses.build
    # end
    respond_modal_with @client#.client_addresses.build
  end

  # GET /clients/1/edit
  def edit
    respond_modal_with @client
  end

  # POST /clients
  # POST /clients.json
  def create
    @client = Client.create(client_params)
    if @client.save
      respond_modal_with @client, location: clients_path
    end
  end

  # PATCH/PUT /clients/1
  # PATCH/PUT /clients/1.json
  def update
    respond_to do |format|
      if @client.update(client_params)
        format.html { redirect_to @client, notice: 'Client was successfully updated.' }
        format.json { render :show, status: :ok, location: clients_path }
      else
        format.html { render :edit }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    @client.destroy
    respond_to do |format|
      format.html { redirect_to clients_url, notice: 'Client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_params
      params.require(:client).permit(:first_name, :last_name, :dni_number, :cuil_number, :email, :status, :mobile_phone, :home_phone, :other_phone, client_addresses_attributes: [:id, :clients_id, :street_name, :street_number, :monoblock, :floor, :department_number, :_destroy, :city_id])
    end
end
