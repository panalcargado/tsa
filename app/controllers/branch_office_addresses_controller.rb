class BranchOfficeAddressesController < ApplicationController
  before_action :set_branch_office_address, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @branch_office_addresses = BranchOfficeAddress.all
    respond_with(@branch_office_addresses)
  end

  def show
    respond_with(@branch_office_address)
  end

  def new
    @branch_office_address = BranchOfficeAddress.new
    respond_with(@branch_office_address)
  end

  def edit
  end

  def create
    @branch_office_address = BranchOfficeAddress.new(branch_office_address_params)
    @branch_office_address.save
    respond_with(@branch_office_address)
  end

  def update
    @branch_office_address.update(branch_office_address_params)
    respond_with(@branch_office_address)
  end

  def destroy
    @branch_office_address.destroy
    respond_with(@branch_office_address)
  end

  private
    def set_branch_office_address
      @branch_office_address = BranchOfficeAddress.find(params[:id])
    end

    def branch_office_address_params
      params.require(:branch_office_address).permit(:street_name, :street_number, :monoblock, :floor, :department_number, :city_id, :branch_office_id)
    end
end
