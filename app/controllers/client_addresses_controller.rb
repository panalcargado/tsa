class ClientAddressesController < ApplicationController
  #before_action :set_client_address, only: [:show, :edit, :update, :destroy]

  #respond_to :html

  def index
    @client_addresses = ClientAddress.joins(:city, :client).all
    #respond_with(@client_addresses)
  end

  def show
    #respond_with(@client_address)
  end

  def new
    @client_address = ClientAddress.new
    #respond_with(@client_address)
    # @coutries = Country.new
    # @states = State.new
    # @cities = City.new
  end

  def edit
  end

  def create
    @client_address = ClientAddress.new(client_address_params)
    respond_to do |format|
      if @client_address.save
        format.html { redirect_to @client_address, notice: 'Client address was successfully created.'}
        format.json { render :show, status: :created, location: @client_address }
      else
        format.html { render :new }
        format.json { render json: @client_address.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @client_address.update(client_address_params)
        format.html { redirect_to @client_address, notice: 'Client address was successfully updated.' }
        format.json { render :show, status: :ok, location: @client_address }
      else
        format.html { render :edit }
        format.json { render json: @client_address.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @client_address.destroy
    respond_to do |format|
      format.html { redirect_to client_address_url, notice: 'Client address was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_client_address
      @client_address = ClientAddress.find(params[:id])
    end

    def client_address_params
      params.require(:client_address).permit(:street_name, :street_number, :monoblock, :floor, :department_number, :client_id, :city_id)
    end
end
