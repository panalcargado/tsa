class BranchOfficesController < ApplicationController
  before_action :set_branch_office, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @branch_offices = BranchOffice.all
    respond_with(@branch_offices)
  end

  def show
    respond_with(@branch_office)
  end

  def new
    @branch_office = BranchOffice.new
    respond_with(@branch_office)
  end

  def edit
  end

  def create
    @branch_office = BranchOffice.new(branch_office_params)
    @branch_office.save
    respond_with(@branch_office)
  end

  def update
    @branch_office.update(branch_office_params)
    respond_with(@branch_office)
  end

  def destroy
    @branch_office.destroy
    respond_with(@branch_office)
  end

  private
    def set_branch_office
      @branch_office = BranchOffice.find(params[:id])
    end

    def branch_office_params
      params.require(:branch_office).permit(:name, :cuit_number, :email, :status, :mobile_phone, :home_phone, :other_phone, :client_id)
    end
end
