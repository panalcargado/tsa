json.extract! client_address, :id, :street_name, :street_number, :monoblock, :floor, :department_number, :city_id, :client_id, :created_at, :updated_at
json.url client_address_url(client_address, format: :json)
