json.extract! branch_office_address, :id, :street_name, :street_number, :monoblock, :floor, :department_number, :city_id, :branch_office_id, :created_at, :updated_at
json.url branch_office_address_url(branch_office_address, format: :json)
