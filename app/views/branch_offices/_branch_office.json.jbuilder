json.extract! branch_office, :id, :name, :cuit_number, :email, :status, :mobile_phone, :home_phone, :other_phone, :client_id, :created_at, :updated_at
json.url branch_office_url(branch_office, format: :json)
