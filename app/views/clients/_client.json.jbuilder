json.extract! client, :id, :first_name, :last_name, :dni_number, :email, :status, :mobile_phone, :home_phone, :other_phone, :created_at, :updated_at
json.url client_url(client, format: :json)
