require 'test_helper'

class BranchOfficeAddressesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @branch_office_address = branch_office_addresses(:one)
  end

  test "should get index" do
    get branch_office_addresses_url
    assert_response :success
  end

  test "should get new" do
    get new_branch_office_address_url
    assert_response :success
  end

  test "should create branch_office_address" do
    assert_difference('BranchOfficeAddress.count') do
      post branch_office_addresses_url, params: { branch_office_address: { branch_office_id: @branch_office_address.branch_office_id, city_id: @branch_office_address.city_id, department_number: @branch_office_address.department_number, floor: @branch_office_address.floor, monoblock: @branch_office_address.monoblock, street_name: @branch_office_address.street_name, street_number: @branch_office_address.street_number } }
    end

    assert_redirected_to branch_office_address_url(BranchOfficeAddress.last)
  end

  test "should show branch_office_address" do
    get branch_office_address_url(@branch_office_address)
    assert_response :success
  end

  test "should get edit" do
    get edit_branch_office_address_url(@branch_office_address)
    assert_response :success
  end

  test "should update branch_office_address" do
    patch branch_office_address_url(@branch_office_address), params: { branch_office_address: { branch_office_id: @branch_office_address.branch_office_id, city_id: @branch_office_address.city_id, department_number: @branch_office_address.department_number, floor: @branch_office_address.floor, monoblock: @branch_office_address.monoblock, street_name: @branch_office_address.street_name, street_number: @branch_office_address.street_number } }
    assert_redirected_to branch_office_address_url(@branch_office_address)
  end

  test "should destroy branch_office_address" do
    assert_difference('BranchOfficeAddress.count', -1) do
      delete branch_office_address_url(@branch_office_address)
    end

    assert_redirected_to branch_office_addresses_url
  end
end
