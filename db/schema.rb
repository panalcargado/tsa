# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170426025526) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "branch_office_addresses", force: :cascade do |t|
    t.string   "street_name"
    t.integer  "street_number"
    t.string   "monoblock"
    t.string   "floor"
    t.integer  "department_number"
    t.integer  "city_id"
    t.integer  "branch_office_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["branch_office_id"], name: "index_branch_office_addresses_on_branch_office_id", using: :btree
    t.index ["city_id"], name: "index_branch_office_addresses_on_city_id", using: :btree
  end

  create_table "branch_offices", force: :cascade do |t|
    t.string   "name"
    t.integer  "cuit_number"
    t.string   "email"
    t.string   "status"
    t.integer  "mobile_phone"
    t.integer  "home_phone"
    t.integer  "other_phone"
    t.integer  "client_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["client_id"], name: "index_branch_offices_on_client_id", using: :btree
  end

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.integer  "state_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["state_id"], name: "index_cities_on_state_id", using: :btree
  end

  create_table "client_addresses", force: :cascade do |t|
    t.string   "street_name"
    t.integer  "street_number"
    t.string   "monoblock"
    t.string   "floor"
    t.integer  "department_number"
    t.integer  "city_id"
    t.integer  "client_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["city_id"], name: "index_client_addresses_on_city_id", using: :btree
    t.index ["client_id"], name: "index_client_addresses_on_client_id", using: :btree
  end

  create_table "clients", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "dni_number"
    t.string   "email"
    t.string   "status"
    t.string   "mobile_phone"
    t.string   "home_phone"
    t.string   "other_phone"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "cuil_number"
  end

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "states", force: :cascade do |t|
    t.string   "name"
    t.integer  "country_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country_id"], name: "index_states_on_country_id", using: :btree
  end

  create_table "tasks", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.datetime "due_date"
    t.string   "status"
    t.string   "priority"
    t.string   "category"
    t.boolean  "private",     default: false
    t.datetime "start_date"
  end

  create_table "user_tasks", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["task_id"], name: "index_user_tasks_on_task_id", using: :btree
    t.index ["user_id"], name: "index_user_tasks_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",     null: false
    t.string   "encrypted_password",     default: "",     null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "role",                   default: "user"
    t.boolean  "status",                 default: true
    t.string   "avatar"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,      null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.string   "invited_by_type"
    t.integer  "invited_by_id"
    t.integer  "invitations_count",      default: 0
    t.datetime "confirmed_at"
    t.string   "confirmation_token"
    t.datetime "confirmation_sent_at"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
    t.index ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "branch_office_addresses", "branch_offices"
  add_foreign_key "branch_office_addresses", "cities"
  add_foreign_key "branch_offices", "clients"
  add_foreign_key "cities", "states"
  add_foreign_key "client_addresses", "cities"
  add_foreign_key "client_addresses", "clients"
  add_foreign_key "states", "countries"
  add_foreign_key "user_tasks", "tasks"
  add_foreign_key "user_tasks", "users"
end