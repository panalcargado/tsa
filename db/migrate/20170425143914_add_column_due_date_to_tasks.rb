class AddColumnDueDateToTasks < ActiveRecord::Migration[5.0]
  def change
  	add_column :tasks, :due_date, :datetime
  	add_column :tasks, :status, :string
  	add_column :tasks, :priority, :string
  	add_column :tasks, :category, :string
  end
end
