class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients do |t|
      t.string :first_name
      t.string :last_name
      t.string :dni_number
      t.string :email
      t.string :status
      t.string :mobile_phone
      t.string :home_phone
      t.string :other_phone

      t.timestamps
    end
  end
end
