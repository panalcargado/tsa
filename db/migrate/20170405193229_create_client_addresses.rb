class CreateClientAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :client_addresses do |t|
      t.string :street_name
      t.integer :street_number
      t.string :monoblock
      t.string :floor
      t.integer :department_number
      t.references :city, foreign_key: true
      t.references :client, foreign_key: true

      t.timestamps
    end
  end
end
