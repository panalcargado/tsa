class CreateBranchOffices < ActiveRecord::Migration[5.0]
  def change
    create_table :branch_offices do |t|
      t.string :name
      t.integer :cuit_number
      t.string :email
      t.string :status
      t.integer :mobile_phone
      t.integer :home_phone
      t.integer :other_phone
      t.references :client, foreign_key: true

      t.timestamps
    end
  end
end
