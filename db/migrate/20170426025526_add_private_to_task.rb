class AddPrivateToTask < ActiveRecord::Migration[5.0]
  def change
  	add_column :tasks, :private, :boolean, default: false
  	add_column :tasks, :start_date, :datetime
  end
end
