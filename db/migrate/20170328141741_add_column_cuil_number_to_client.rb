class AddColumnCuilNumberToClient < ActiveRecord::Migration[5.0]
  def change
  	add_column :clients, :cuil_number, :integer
  end
end
