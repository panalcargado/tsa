require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Tsa
  class Application < Rails::Application
    # Use the responders controller from the responders gem
    config.app_generators.scaffold_controller :responders_controller

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.active_record.raise_in_transactional_callbacks = true

    config.active_job.queue_adapter = :delayed_job
    config.action_mailer.default_url_options = { host: 'smtp.mailgun.org' }

    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
    	address: 'localhost',
    	port: 1025

        # address: '184.173.153.207',
        # port: 587,
        # domain: 'sandboxc8656caa2e0346bc8cb461eefb28c689.mailgun.org',
        # user_name: 'postmaster@sandboxc8656caa2e0346bc8cb461eefb28c689.mailgun.org',
        # password: '539f27c69e28f5e7bea4ca125a996d31'
    }  
  end
end
