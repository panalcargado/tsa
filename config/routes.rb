Rails.application.routes.draw do
  resources :tasks
  devise_for :users
  resources :users, :controller => "users"
  resources :branch_office_addresses
  resources :branch_offices
  resources :client_addresses
  resources :clients
  resources :cities
  resources :states
  resources :countries

  root to: "home#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
